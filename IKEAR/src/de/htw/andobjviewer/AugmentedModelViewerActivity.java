package de.htw.andobjviewer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Debug;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.TextView;
import android.widget.Toast;
import de.htw.andarmodelviewer.R;
import de.htw.andobjviewer.graphics.LightingRenderer;
import de.htw.andobjviewer.graphics.Model3D;
import de.htw.andobjviewer.models.Material;
import de.htw.andobjviewer.models.Model;
import de.htw.andobjviewer.parser.ObjParser;
import de.htw.andobjviewer.parser.ParseException;
import de.htw.andobjviewer.util.AssetsFileUtil;
import de.htw.andobjviewer.util.BaseFileUtil;
import de.htw.andobjviewer.util.MaterialFileReader;
import de.htw.andobjviewer.util.ProductInfo;
import de.htw.andobjviewer.util.ReadXMLFile;
import edu.dhbw.andar.ARToolkit;
import edu.dhbw.andar.AndARActivity;
import edu.dhbw.andar.exceptions.AndARException;

/**
 * Example of an application that makes use of the AndAR toolkit.
 * @author Daniel Goertz & Katrin Baewert
 *
 */
public class AugmentedModelViewerActivity extends AndARActivity implements SurfaceHolder.Callback {
	
	//View a file in the assets folder
	public static final int TYPE_INTERNAL = 0;


	public static final boolean DEBUG = false;
	
	/* Menu Options: */
	private final int MENU_SCALE = 0;
	private final int MENU_ROTATE = 1;
	private final int MENU_TRANSLATE = 2;
	private final int MENU_SCREENSHOT = 3;
	private final int MENU_INFO = 4;
	
	private int mode = MENU_SCALE;

	private Model model;
	private Model3D model3d;
	private Model3D second_model3d;
	private ProgressDialog waitDialog;
	private Dialog simpleDialog;
	private Resources res;

	private String modelFileName;
	
	ARToolkit artoolkit;
	
	public AugmentedModelViewerActivity() {
		super(false);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setNonARRenderer(new LightingRenderer());//or might be omited
		res=getResources();
		artoolkit = getArtoolkit();		
		getSurfaceView().setOnTouchListener(new TouchEventHandler());
		getSurfaceView().getHolder().addCallback(this);
	}
	
	

	/**
	 * Inform the user about exceptions that occurred in background threads.
	 */
	@Override
	public void uncaughtException(Thread thread, Throwable ex) {
		System.out.println("uncaughtException(Thread, Throwable)");
	}
	

    /** create the menu
     * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	menu.add(0, MENU_TRANSLATE, 0, res.getText(R.string.translate))
    		.setIcon(R.drawable.translate);
        menu.add(0, MENU_ROTATE, 0, res.getText(R.string.rotate))
        	.setIcon(R.drawable.rotate);
        menu.add(0, MENU_SCALE, 0, res.getText(R.string.scale))
        	.setIcon(R.drawable.scale);     
        menu.add(0, MENU_SCREENSHOT, 0, res.getText(R.string.take_screenshot))
    		.setIcon(R.drawable.screenshoticon);   
        menu.add(0, MENU_INFO, 0, res.getText(R.string.info)).setIcon(R.drawable.info); 
        return true;
    }
    
    /** Handles item selections */
    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
	        case MENU_SCALE:
	            mode = MENU_SCALE;
	            return true;
	        case MENU_ROTATE:
	        	mode = MENU_ROTATE;
	            return true;
	        case MENU_TRANSLATE:
	        	mode = MENU_TRANSLATE;
	            return true;
	        case MENU_SCREENSHOT:
	        	waitDialog = ProgressDialog.show(AugmentedModelViewerActivity.this, "", 
	        			 getResources().getText(R.string.loading), true);
	        	waitDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
	        	waitDialog.show();
	        	new TakeAsyncScreenshot().execute();
	        	return true;       	
	        case MENU_INFO:  
	        	simpleDialog = new Dialog(AugmentedModelViewerActivity.this);
	        	simpleDialog.show();
	        	simpleDialog.setContentView(R.layout.info);
	        	new ShowInfo().execute();
	        	return true;	
	    }
        return false;
    }
    
    @Override
    public void surfaceCreated(SurfaceHolder holder) {
    	super.surfaceCreated(holder);
    	/*load the model
    	this is done here, to assure the surface was already created, 
    	so that the preview can be started after loading the model */
    	if(model == null) {
			waitDialog = ProgressDialog.show(this, "", 
	                getResources().getText(R.string.loading), true);
			waitDialog.show();
			new ModelLoader().execute();
		}
    }
    
	
    /**
     * Handles touch events.
     * @author Katrin Baewert, Daniel Goertz
     */
    class TouchEventHandler implements OnTouchListener {
    	
    	private float lastX=0;
    	private float lastY=0;

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			if(model!=null) {
				switch(event.getAction()) {
					//Action started
					default:
					case MotionEvent.ACTION_DOWN:
						lastX = event.getX();
						lastY = event.getY();
						break;
					//Action ongoing
					case MotionEvent.ACTION_MOVE:
						float dX = lastX - event.getX();
						float dY = lastY - event.getY();
						lastX = event.getX();
						lastY = event.getY();
						if(model != null) {
							switch(mode) {
								case MENU_SCALE:
									model.setScale(dY/100.0f);
						            break;
						        case MENU_ROTATE:
						        	model.setXrot(-1*dX);//dY-> Rotation um die X-Achse
									model.setYrot(-1*dY);//dX-> Rotation um die Y-Achse
						            break;
						        case MENU_TRANSLATE:
						        	model.setXpos(dY/10f);
									model.setYpos(dX/10f);
						        	break;						    
							}		
						}
						break;
					//Action ended
					case MotionEvent.ACTION_CANCEL:	
					case MotionEvent.ACTION_UP:
						lastX = event.getX();
						lastY = event.getY();
						break;
				}
			}
			return true;
		}
    	
    }
    
    /**
     * Load the Model
     * @author Katrin Baewert, Daniel Goertz
     */
	private class ModelLoader extends AsyncTask<Void, Void, Void> {
			
    	@Override
    	protected Void doInBackground(Void... params) {
    		
			Intent intent = getIntent();
			Bundle data = intent.getExtras();
			int type = data.getInt("type");
			modelFileName = data.getString("name");
			int[] colorArray = data.getIntArray("color"); 
			String filename = data.getString("filename");
			
			boolean hasColor = false;
			float [] fc = {0f,0f,0f}; //set default material diffuse
					
			BaseFileUtil fileUtil= null;
			File modelFile=null;
			
			fileUtil = new AssetsFileUtil(getResources().getAssets());
			fileUtil.setBaseFolder("models/");
	
			//read the model file:						
			if(modelFileName.endsWith(".obj")) {
				ObjParser parser = new ObjParser(fileUtil);
				try {
					if(Config.DEBUG) // debugging
						Debug.startMethodTracing("AndObjViewer");
					if(fileUtil != null) {  //path correct, file exists
						BufferedReader fileReader = fileUtil.getReaderFromName(modelFileName);
						
						if(fileReader != null) {
							
							//set material
							//***************************************		
							for(int i=0; i< colorArray.length; i++){
								if (colorArray[i] >0) {
									  if(colorArray[i]>100){
										  hasColor = false;
										  break;
									  }else {
										  fc[i] = colorArray[i] /100f;
										  hasColor = true; 
									  }
								}else  fc[i] = 0.0f;
							} 
							
							// if material color should change:
							if(hasColor){
								AssetManager assetManager = getAssets();
								
								//read material
								MaterialFileReader mfr = new MaterialFileReader();
								HashMap<String, Material> materialMap = new HashMap<String, Material>();
								
								//get the hash map with material
								materialMap= mfr.changeMaterial(filename +".mtl",fc, assetManager);
								
								//parse model
								model = parser.parse("Model", fileReader);
								
								//set new Material
								model.setMaterial (materialMap);
								
								//output HashMap for Testing
								for(HashMap.Entry<String, Material> e : materialMap.entrySet()){
									  String s = e.getKey();
									  Material d = e.getValue();
									  //model.addMaterial(d);
									  Log.i("INFO", s + d.toString());
									}
							
							} else { 	//only parse model
								model = parser.parse("Model", fileReader);
							}
							
							//model that the user selected
							model3d = new Model3D(model);
							
							//second model
							second_model3d = new Model3D(model, "Model","ikear.patt", 80.0, new double[]{0,0});
						}
					}
					if(Config.DEBUG)
						Debug.stopMethodTracing();
				} catch (IOException ex) {
					ex.printStackTrace();
					Log.e("ERROR", "IO Exception");
				} catch (ParseException ex) {
					ex.printStackTrace();
					Log.e("ERROR", "ParseException");
				}
			}
    		return null;
    	}
    	@Override
    	protected void onPostExecute(Void result) {
    		super.onPostExecute(result);
    		waitDialog.dismiss();
    		
    		//register model
    		try {
    			if(model3d!=null) 
    				artoolkit.registerARObject(model3d);
    			//register second model
    			if(second_model3d!=null) 
    				artoolkit.registerARObject(second_model3d);
    			
			} catch (AndARException e) {
				e.printStackTrace();
			}
			startPreview();
    	}
    }
	
	/**
	 * Show the product info for the shown model
	 * @author Katrin Baewert
	 *
	 */
	class ShowInfo extends AsyncTask<Void, Void, Void> {
		
		private String errorMsg = null;
		
		@Override
		protected Void doInBackground(Void... arg0) {
			return null;
		}
		
		
		protected void onPostExecute(Void result) {
			
			Intent intent = getIntent();
			Bundle data = intent.getExtras();
			
			ReadXMLFile readXmlFile = new ReadXMLFile();
        	ProductInfo pinfo =  new ProductInfo();   
        	
        	modelFileName= data.getString("name");
        	AssetManager assetManager = getResources().getAssets();			
			
			pinfo = readXmlFile.parseXML(modelFileName,assetManager);
			
			TextView text_label = (TextView) simpleDialog.findViewById(R.id.text_info_label);
			text_label.setText("NAME: " + pinfo.getName());
			
			TextView text_size = (TextView) simpleDialog.findViewById(R.id.text_info_size);
			text_size.setText(" HÖHE: "+ pinfo.getHight()+" cm, BREITE: " + pinfo.getWidth() + "cm, TIEFE: "+ pinfo.getDeep()+" cm ");

			TextView text_price = (TextView) simpleDialog.findViewById(R.id.text_info_price);
        	text_price.setText(" PREIS:" + pinfo.getPrice() + " € ");
        	
        	TextView text_weigth = (TextView) simpleDialog.findViewById(R.id.text_info_weigth);
        	text_weigth.setText(" GEWICHT: " + pinfo.getWeigth() + " kg ");
        	
        	TextView text_product_id = (TextView) simpleDialog.findViewById(R.id.text_product_id);
        	text_product_id.setText(" ARTIKELNUMMER: " + pinfo.getId());
		};
		
	}
	
	/**
	 * Take a screenshot
	 * @author Katin Baewert
	 *
	 */
	class TakeAsyncScreenshot extends AsyncTask<Void, Void, Void> {
		
		private String errorMsg = null;

		@Override
		protected Void doInBackground(Void... params) {
			Bitmap bm = takeScreenshot();
			FileOutputStream fos;
			try {			
				fos = new FileOutputStream("/sdcard/AndARScreenshot"+new Date().getTime()+".png");
				bm.compress(CompressFormat.PNG, 100, fos);
				fos.flush();
				fos.close();
			} catch (FileNotFoundException e) {
				errorMsg = e.getMessage();
				e.printStackTrace();
			} catch (IOException e) {
				errorMsg = e.getMessage();
				e.printStackTrace();
			}	
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			waitDialog.dismiss();
			if(errorMsg == null)
				Toast.makeText(AugmentedModelViewerActivity.this, getResources().getText(R.string.screenshotsaved), Toast.LENGTH_SHORT ).show();
			else
				Toast.makeText(AugmentedModelViewerActivity.this, getResources().getText(R.string.screenshotfailed)+errorMsg, Toast.LENGTH_SHORT ).show();
		};
		
	}
	
	
}