package de.htw.andobjviewer;

import de.htw.andarmodelviewer.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/** 
 * Start Activity
 * @author Daniel Goertz & Katrin Baewert
 */
public class StartActivity extends Activity {
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_layout);

        Button next = (Button) findViewById(R.id.button_changeItem);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
			public void onClick(View view) {
                Intent myIntent = new Intent(view.getContext(), ModelChooser.class);
                startActivityForResult(myIntent, 0);                    	
            }

        });
        
        Button exit = (Button) findViewById(R.id.button_exit);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
			public void onClick(View view) {
            	finish();
            }

        });
    }

}
