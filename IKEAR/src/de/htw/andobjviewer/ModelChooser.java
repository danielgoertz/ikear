package de.htw.andobjviewer;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import de.htw.andarmodelviewer.R;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

/**
 * This class create Item 
 * @author Daniel Goertz & Katrin Baewert
 *
 */
public class ModelChooser extends ListActivity {
	
	int[] colorArray = {200,200,200};//{50, 0, 100};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		AssetManager am = getAssets();
		Vector<Item> models = new Vector<Item>();
		Item item = new Item();	
		
		item = new Item();
		item.text = getResources().getString(R.string.instructions);
		item.icon = new Integer(R.drawable.help);
		models.add(item);
		
		item = new Item();
		item.text = getResources().getString(R.string.selected_color_label);
		item.type = Item.TYPE_HEADER;
		models.add(item);
		
		try {
			String[] modelFiles = am.list("models");
			List<String> modelFilesList = Arrays.asList(modelFiles);
			
			String[] colorFiles = am.list("colors");
			List<String> colorFilesList = Arrays.asList(colorFiles);
			
			// list all color icons
			for (String currFile : colorFiles) {					
					String currFileName = currFile;				
					if (currFileName.endsWith(".png")) {
						item = new Item();
						String trimmedFileName = currFileName.substring(0,currFileName.lastIndexOf(".png"));
						item.text = trimmedFileName;
						item.type = Item.TYPE_COLOR;
						models.add(item);
						if (colorFilesList.contains(trimmedFileName + ".png")) {
							InputStream is = am.open("colors/"+ trimmedFileName + ".png");
							item.icon = (BitmapFactory.decodeStream(is));
						}
					}		
			}			
			
			item = new Item();
			item.text = getResources().getString(R.string.selected_color);
			item.type = Item.TYPE_SELECT_COLOR;
			models.add(item);
			
			item = new Item();
			item.text = getResources().getString(R.string.choose_a_model);
			item.type = Item.TYPE_HEADER;
			models.add(item);
			
			// list all icons of the 3d-models
			for (String currFile : modelFiles) {
				String currFileName = currFile;
				if(currFileName.endsWith(".obj")) { 
					item = new Item();
					String trimmedFileName = currFileName.substring(0,currFileName.lastIndexOf(".obj"));					
					item.text = trimmedFileName;
					models.add(item);
					if(modelFilesList.contains(trimmedFileName+".png")) {
						InputStream is = am.open("models/"+trimmedFileName+".png");
						item.icon=(BitmapFactory.decodeStream(is));
					} 
				}
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		setListAdapter(new ModelChooserListAdapter(models));		

	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		Item item = (Item) this.getListAdapter().getItem(position);
		String str = item.text;
		
		//Default Color values >100 are not allowed, it implies no color selected
		boolean hasColor = false;
		
		if(str.equals("BEIGE")){			
			TextView color_text = (TextView) findViewById(R.id.list_selected_color);
        	if(color_text!=null){
        		color_text.setText("Gewählt: " + str);
        		colorArray[0] = 90;
        		colorArray[1] = 80;
        		colorArray[2] = 70;
        	}
		}
		if(str.equals("SCHWARZ")){
			TextView color_text = (TextView) findViewById(R.id.list_selected_color);
        	if(color_text!=null){
        		color_text.setText("Gewählt: " + str);
	       		colorArray[0] = 13;
	    		colorArray[1] = 13;
	    		colorArray[2] = 13;
        	}	
		}
		if(str.equals("BRAUN")){
			TextView color_text = (TextView) findViewById(R.id.list_selected_color);
        	if(color_text!=null){
        		color_text.setText("Gewählt: " + str);
	       		colorArray[0] = 14;
	    		colorArray[1] = 7;
	    		colorArray[2] = 2;
        	}			
		}
		if(str.equals(getResources().getString(R.string.instructions))) {
			//show up the instructions activity
			startActivity(new Intent(ModelChooser.this, InstructionsActivity.class));
		}
		if( (str.equals("BOERJE")) || (str.equals("EKTORP")) || (str.equals("EXPEDIT")) ) {
			//load the selected internal file
			Intent intent = new Intent(ModelChooser.this, AugmentedModelViewerActivity.class);
            intent.putExtra("name", str+".obj");
          
            //add changed material
            // load the selected color
            if(colorArray == null)
            	colorArray[0] = 200;
            
            intent.putExtra ("color", colorArray);
            
            intent.putExtra ("filename", str);
            
            intent.putExtra("type", AugmentedModelViewerActivity.TYPE_INTERNAL);
            intent.setAction(Intent.ACTION_VIEW);
 
            startActivity(intent);
		} 					
	}
	
	class ModelChooserListAdapter extends BaseAdapter{
		
		private Vector<Item> items;
		
		public ModelChooserListAdapter(Vector<Item> items) {
			this.items = items;
		}
		

		@Override
		public int getCount() {
			return items.size();
		}

		@Override
		public Object getItem(int position) {
			return items.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}
		
		@Override
		public int getViewTypeCount() {
			//normal items, and the header
			return 2;
		}
		
		@Override
		public boolean areAllItemsEnabled() {
			return false;
		}
		
		@Override
		public boolean isEnabled(int position) {
			return !(items.get(position).type==Item.TYPE_HEADER);
		}
		
		@Override
		public int getItemViewType(int position) {
			return items.get(position).type;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;
			Item item = items.get(position);
            if (v == null) {
            	LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            	switch(item.type) { 
            	case Item.TYPE_SELECT_COLOR:            		
                    v = vi.inflate(R.layout.list_color, null);
            		break;
            	case Item.TYPE_HEADER:            		
                    v = vi.inflate(R.layout.list_header, null);
            		break;     
            	case Item.TYPE_ITEM:
            		v = vi.inflate(R.layout.choose_model_row, null);
            		break;
            	case Item.TYPE_COLOR:
            		v = vi.inflate(R.layout.list_colors, null);
            		break;
            	}        
            }   
            if(item != null) {
	            switch(item.type) {            
	        	case Item.TYPE_SELECT_COLOR: 
	        		TextView selectColor = (TextView) v.findViewById(R.id.list_selected_color);
	        		if(selectColor != null) {
	        			selectColor.setText(item.text);
	        		}
	        	case Item.TYPE_HEADER: 
	        		TextView headerText = (TextView) v.findViewById(R.id.list_header_title);
	        		if(headerText != null) {
	        			headerText.setText(item.text);
	        		}
	        		break;
	        	case Item.TYPE_ITEM:
	        		Object iconImage = item.icon;
	            	ImageView icon = (ImageView) v.findViewById(R.id.choose_model_row_icon);
	            	if(icon!=null) {
	            		if(iconImage instanceof Integer) {
	            			icon.setImageResource(((Integer)iconImage).intValue());
	            		} else if(iconImage instanceof Bitmap) {
	            			icon.setImageBitmap((Bitmap)iconImage);
	            		}
	            	}
	            	TextView text = (TextView) v.findViewById(R.id.choose_model_row_text);
	            	if(text!=null)
	            		text.setText(item.text);	            		       	
	        		break;
	        		
	        	case Item.TYPE_COLOR:
	        		Object iconColor = item.icon;
	            	ImageView imageViewIcon = (ImageView) v.findViewById(R.id.color_icon);
	            	if(imageViewIcon!=null) {
	            		if(iconColor instanceof Integer) {
	            			imageViewIcon.setImageResource(((Integer)iconColor).intValue());
	            		} else if(iconColor instanceof Bitmap) {
	            			imageViewIcon.setImageBitmap((Bitmap)iconColor);
	            		}
	            	}
	            	TextView color_text = (TextView) v.findViewById(R.id.color_icon_row_text);
	            	if(color_text!=null)
	            		color_text.setText(item.text); 
	        		break;	        		
	        	}      
            }
			return v;
		}
		
	}
	
	class Item {
		public static final int TYPE_ITEM=0;
		public static final int TYPE_HEADER=1;
		public static final int TYPE_COLOR=2;
		public static final int TYPE_SELECT_COLOR=3;
		public int type = TYPE_ITEM;
		public Object icon;
		public String text;
	}
	
}
