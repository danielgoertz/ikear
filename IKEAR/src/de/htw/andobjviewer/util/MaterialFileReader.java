package de.htw.andobjviewer.util;
import java.io.*;
import java.util.HashMap;
import java.util.StringTokenizer;

import android.content.res.AssetManager;
import android.util.Log;

import de.htw.andobjviewer.models.Material;

/**
 * Class for reading material file and change material.
 * @author Katrin Baewert
 *
 */
public class MaterialFileReader {
	

	private BaseFileUtil fileUtil;
	public String filepath = "";
	public String filename = ".mtl";
	
	
	public MaterialFileReader() {
		
	}
	 

	    /**
	     * MTL file format:
	     *
	     * <PRE>
	     * 		newmtl white          // begin material and specify name
	     * 		Kd 1.0 1.0 1.0        // diffuse rgb
	     * 		Ka 0.2 0.2 0.2        // ambient rgb
	     * 		Ks 0.6 0.6 0.6        // specular rgb
	     * 		Ns 300                // shininess 0-1000
	     *      d 0.5                 // alpha 0-1
	     *      map_Kd texture.jpg    // texture file
	     *                            // blank line ends material definition
	     * </PRE>
	     */    
	    public HashMap<String, Material> changeMaterial(String modelFile, float [] colour, AssetManager as) {
	 	   
	    	HashMap<String, Material> materialMap = new HashMap<String, Material>(); //HashMap for material
	    	String line = "";
	    	Material curMat = new Material("default"); //initialize with default
	    	
			try {
				
				fileUtil = new AssetsFileUtil(as);				
				fileUtil.setBaseFolder("models/");
				BufferedReader br = fileUtil.getReaderFromName(modelFile);
			
	    	float[] rgb;
	    		while ((line = br.readLine()) != null) {
	    			// remove extra whitespace
	    			line = line.trim();
	    			if (line.length() > 0) {
	    				if (line.startsWith("#")) {
	    					// ignore comments
	    				}
	    				else if (line.startsWith("newmtl")) {
	    					// newmtl some_name
	    					String mtlName = line.substring(7);
	    					curMat = new Material(mtlName);  // start new material
	    					materialMap.put(mtlName,curMat);      // add to list
	    				}
	    				else if (line.startsWith("Kd")) { //set diffuse --> colour
	    					// Kd 1.0 0.0 0.5
	    					if ((rgb = read3Floats(line)) != null) {
	    						curMat.setDiffuse(colour);
	    					}
	    				}
	    				else if (line.startsWith("Ka")) {
	    					// Ka 1.0 0.0 0.5
	    					if ((rgb = read3Floats(line)) != null) {
	    						curMat.setAmbient(rgb);
	    					}
	    				}
	    				else if (line.startsWith("Ks")) {
	    					// Ks 1.0 0.0 0.5
	    					if ((rgb = read3Floats(line)) != null) {
	    						curMat.setSpecular(rgb);
	    					}
	    				}
	    				else if (line.startsWith("Ns")) {
	    					// Ns 500.5
	    					// shininess in mtl file is 0-1000
	    					if ((rgb = read3Floats(line)) != null) {
	    						// convert to opengl 0-127
	    						int shininessValue = (int) ((rgb[0] / 1000f) * 127f);
	    						curMat.setShininess( shininessValue );
	    					}
	    				}
	    				else if (line.startsWith("d")) {
	    					// d 1.0
	    					// alpha value of material 0=transparent 1=opaque
	    					if ((rgb = read3Floats(line)) != null) {
	    						curMat.setAlpha( rgb[0] );
	    					}
	    				}
	    				else if (line.startsWith("illum")) {
	    					// illum (0, 1, or 2)
	    					// lighting for material 0=disable, 1=ambient & diffuse (specular is black), 2 for full lighting.
	    					if ((rgb = read3Floats(line)) != null) {
	    						// not yet
	    					}
	    				}
	    			}
	    		}
	    	} catch (FileNotFoundException e1) {
				e1.printStackTrace();
	    	} catch (Exception e) {
	    		System.out.println("MatrialFileReader.loadMaterials() failed at line: " + line);
	    		Log.d("DEBUG", "failed loading and changing materials");
	    	}
				
	    	// debug infos:
	    	System.out.println("MatrialFileReader.loadMaterials(): loaded " + materialMap.size() + " materials ");
	    	Log.d("INFO", "MatrialFileReader.loadMaterials(): loaded " + materialMap.size() + " materials ");
	    	
	    	
	    	// return HashMap of materials	    	
	    	return materialMap;
	    }
	    

	    // always return array of four floats (usually containing RGBA, but
	    // in some cases contains only one value at pos 0).
	    private float[] read3Floats(String line)
	    {
	    	try
	    	{
	    		StringTokenizer st = new StringTokenizer(line, " ");
	    		st.nextToken();   // throw out line identifier (Ka, Kd, etc.)
	    		if (st.countTokens() == 1) {
	    			return new float[] {Float.parseFloat(st.nextToken()), 0f, 0f, 0f};
	    		}
	    		else if (st.countTokens() == 3) { // RGBA (force A to 1)
	    			return new float[] {Float.parseFloat(st.nextToken()),
	    					Float.parseFloat(st.nextToken()),
	    					Float.parseFloat(st.nextToken()),
	    					1f };
	    		}
	    	}
	    	catch (Exception e)
	    	{
	    		System.out.println("MatrialFileReader.changeColour(): error on line '" + line + "', " + e);
	    	}
	    	return null;
	    }	    
}
