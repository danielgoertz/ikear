package de.htw.andobjviewer.util;

import java.util.ArrayList;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

/**
 * Handle XML file reader
 * @author Katrin Baewert
 *
 */
public class ProductInfoHandler extends DefaultHandler { 
		 
		  // booleans that check whether it's in a specific tag or not 
		  private boolean _inName, _inPrice, _inWidth, _inDeep,_inHight,_inWeigth, _inId,_inColour,_inNrOfMaterial;
		 
		  // this holds the data 
		  private ProductInfo prodInfo; 
		  private ArrayList <ProductInfo> productInfos = new ArrayList<ProductInfo> ();
		 
		  /** 
		   * Returns the data object 
		   * 
		   * @return 
		   */ 
		  public ArrayList<ProductInfo> getProductInfos() { 
		    return productInfos; 
		  } 
		 
		  /** 
		   * This gets called when the xml document is first opened 
		   * 
		   * @throws SAXException 
		   */ 
		  @Override 
		  public void startDocument() throws SAXException { 
		  } 
		 
		  /** 
		   * Called when it's finished handling the document 
		   * 
		   * @throws SAXException 
		   */ 
		  @Override 
		  public void endDocument() throws SAXException { 
		 
		  } 
		 
		  /** 
		   * This gets called at the start of an element. Here we're also setting the booleans to true if it's at that specific tag. (so we 
		   * know where we are) 
		   * 
		   * @param namespaceURI 
		   * @param localName 
		   * @param qName 
		   * @param atts 
		   * @throws SAXException 
		   */ 
		  @Override 
		  public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException { 
		 
			if (localName.equals("FURNITURE")) {
				prodInfo = new ProductInfo();
			}
			if (localName.equals("NAME")) {
				_inName = true;
				prodInfo.setName(atts.getValue("NAME"));
	
			} else if (localName.equals("PRICE")) {
				_inPrice = true;
				prodInfo.setPrice(atts.getValue("PRICE"));
				
			} else if (localName.equals("WIDTH")) {
				_inWidth = true;
				prodInfo.setWidth(atts.getValue("WIDTH"));
				
			} else if (localName.equals("DEEP")) {
				_inDeep = true;
				prodInfo.setDeep(atts.getValue("DEEP"));
				
			} else if (localName.equals("HIGHT")) {
				_inHight = true;
				prodInfo.setHight(atts.getValue("HIGHT"));
				
			} else if (localName.equals("ID")) {
				_inId = true;
				prodInfo.setId(atts.getValue("ID"));
				
			} else if (localName.equals("WEIGTH")) {
				_inWeigth = true;
				prodInfo.setWeigth(atts.getValue("WEIGTH"));
				
			}else if (localName.equals("COLOR")) {
				_inColour = true;
				prodInfo.setColour(atts.getValue("COLOR"));
				
			}else if (localName.equals("MATERIALS")) {
				_inNrOfMaterial = true;
				prodInfo.setNrOfMaterial(atts.getValue("MATERIALS"));
			}
	
		}

		  /** 
		   * Called at the end of the element. Setting the booleans to false, so we know that we've just left that tag. 
		   * 
		   * @param namespaceURI 
		   * @param localName 
		   * @param qName 
		   * @throws SAXException 
		   */ 
		  @Override 
		  public void endElement(String namespaceURI, String localName, String qName) throws SAXException { 
		    Log.v("endElement", localName); 
		 
			if (localName.equals("NAME")) {
				_inName = false;
			} else if (localName.equals("PRICE")) {
				_inPrice = false;
			} else if (localName.equals("WIDTH")) {
				_inWidth = false;
			} else if (localName.equals("DEEP")) {
				_inDeep = false;
			} else if (localName.equals("HIGHT")) {
				_inHight = false;
			} else if (localName.equals("ID")) {
				_inId = false;
			} else if (localName.equals("WEIGTH")) {
				_inWeigth = false;
			} else if (localName.equals("COLOR")) {
				_inColour = false;
			} else if (localName.equals("MATERIALS")) {
				_inNrOfMaterial = false;
			}
		    
		    if (localName.equals("FURNITURE")) {
		    	productInfos.add(prodInfo);
		    	System.out.println(prodInfo);
		    }
		  } 
		 
		  /** 
		   * Calling when we're within an element. Here we're checking to see if there is any content in the tags that we're interested in 
		   * and populating it in the Config object. 
		   * 
		   * @param ch 
		   * @param start 
		   * @param length 
		   */ 
		  @Override 
		  public void characters(char ch[], int start, int length) { 
		    String chars = new String(ch, start, length); 
		    chars = chars.trim(); 
		 
		    if(_inName) { 
		    	prodInfo.setName(chars); 	
		    } else if(_inPrice) { 
		    	prodInfo.setPrice(chars); 
		    }
		    else if(_inWidth) { 
		    	prodInfo.setWidth(chars); 
		    }
		    else if(_inDeep) { 
		    	prodInfo.setDeep(chars); 
		    }
		    else if(_inHight) { 
		    	prodInfo.setHight(chars); 
		    }
		    else if(_inId) { 
		    	prodInfo.setId(chars); 
		    }
		    else if(_inWeigth) { 
		    	prodInfo.setWeigth(chars); 
		    }
		    else if(_inColour) { 
		    	prodInfo.setColour(chars); 
		    }
		    else if(_inNrOfMaterial) { 
		    	prodInfo.setNrOfMaterial(chars); 
		    }	    
		  } 
		} 
