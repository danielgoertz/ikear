package de.htw.andobjviewer.util;

import java.util.ArrayList;


/**
 * 
 * @author Daniel Goertz & Katrin Baewert
 *
 */
public class ProductInfo {

	public ProductInfo(){
		ArrayList<ProductInfo> pi = null;
	}
	
	public String name = "BOERJE";
	private String price = "1.0";
	private String width = "79";
	private String deep = "39";
	private String hight = "149";
	private String weigth ="30.7";
	private String id = "302.018.13";
	private String colour = "Birke, Grau, Rot, Wei�, Schwarz";
	private String nrOfMaterial = "2";
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getWidth() {
		return width;
	}
	public void setWidth(String width) {
		this.width = width;
	}
	public String getDeep() {
		return deep;
	}
	public void setDeep(String deep) {
		this.deep = deep;
	}
	public String getHight() {
		return hight;
	}
	public void setHight(String hight) {
		this.hight = hight;
	}
	public String getWeigth() {
		return weigth;
	}
	public void setWeigth(String weigth) {
		this.weigth = weigth;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getColour() {
		return colour;
	}
	public void setColour(String colour) {
		this.colour = colour;
	}
	public String getNrOfMaterial() {
		return nrOfMaterial;
	}
	public void setNrOfMaterial(String nrOfMaterial) {
		this.nrOfMaterial = nrOfMaterial;
	}

	
	
}
