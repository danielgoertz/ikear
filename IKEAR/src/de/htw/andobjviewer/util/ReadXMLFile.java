package de.htw.andobjviewer.util;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import android.content.res.AssetManager;
import android.util.Log;

/**
 * Read a XML file
 * @author Daniel Goertz, Katrin Baewert
 *
 */
public class ReadXMLFile {
	
	private BaseFileUtil fileUtil;
	public String filepath = "";
	public String fileend = ".obj";

	public ReadXMLFile (){
		//do some default stuff
		String filename = "/data/furniture.xml";
	}
	
	/**
	 * read an XLM file and retun the Product information
	 * @param modelFileName
	 * @param as
	 * @return productInformation
	 */
	public ProductInfo parseXML(String modelFileName,AssetManager as) {
		
		ProductInfo productInfo = new ProductInfo();
		ArrayList <ProductInfo> productInfos= null;
		 
		  // sax stuff 
		  try { 
		    SAXParserFactory spf = SAXParserFactory.newInstance(); 
		    SAXParser sp = spf.newSAXParser(); 
		 
		    XMLReader xr = sp.getXMLReader(); 
		 
		    ProductInfoHandler dataHandler = new ProductInfoHandler(); 
		    xr.setContentHandler(dataHandler); 
		    
		    fileUtil = new AssetsFileUtil(as);				
			fileUtil.setBaseFolder("data/");
			xr.parse(new InputSource(fileUtil.getReaderFromName("furniture.xml")));

		    productInfos = dataHandler.getProductInfos();
		    String trimmedFileName = modelFileName.substring(0,modelFileName.lastIndexOf(".obj"));
		    for(ProductInfo info : productInfos){
		    	if(trimmedFileName.toUpperCase().equals( info.getName().toUpperCase()) ){
		    		Log.i(trimmedFileName, "*** GLEICH ***");
		    		return info;
		    	}
		    }
		    
		 
		  } catch(ParserConfigurationException pce) { 
		    Log.e("SAX XML", "sax parse error", pce); 
		  } catch(SAXException se) { 
		    Log.e("SAX XML", "sax error", se); 
		  } catch(IOException ioe) { 
		    Log.e("SAX XML", "sax parse io error", ioe); 
		  } 
		 
		  return productInfo; 
		}
}

